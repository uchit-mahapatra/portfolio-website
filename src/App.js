import "./App.scss";
import About from "./components/About/About";
import Home from "./components/Home/Home";
import Projects from "./components/Projects/Projects";
import Sidebar from "./components/Sidebar/Sidebar";
import Skills from "./components/Skills/Skills";
import Contact from "./components/Contact/Contact";
import { useRef } from "react";
import Footer from "./components/Footer/Footer";

function App() {
  const homeRef = useRef(),
    aboutRef = useRef(),
    skillsRef = useRef(),
    projectsRef = useRef(),
    contactRef = useRef();

  return (
    <div className="App">
      <Sidebar pos={[homeRef, aboutRef, skillsRef, projectsRef, contactRef]} />
      <div ref={homeRef}>
        <Home />
      </div>
      <div ref={aboutRef}>
        <About />
      </div>
      <div ref={skillsRef}>
        <Skills />
      </div>
      <div ref={projectsRef}>
        <Projects />
      </div>
      <div ref={contactRef}>
        <Contact />
      </div>
      <Footer />
    </div>
  );
}

export default App;
