import React from "react";
import "./About.scss";
import ProfilePhoto from "../../assets/profilePhoto.png";
import { FiDownloadCloud } from "react-icons/fi";
function About() {
  return (
    <div className="about" id="about">
      <h1>
        <span>About Me</span>
      </h1>
      <div className="aboutMeContainer">
        <img src={ProfilePhoto} alt="ProfilePhoto" />
        <div className="aboutMeSection">
          <p className="aboutMe">
            Hey, I'm Uchit Mahapatra, a Computer Science graduate with a passion
            for developing web applications that is fast, scalable and
            user-friendly.
          </p>
          <p className="resumeLink">
            Find my resume here{" "}
            <a href="https://drive.google.com/file/d/1Hgmw3_8qzatK-MCmuPYGjVa2BvDuSyCE/view?usp=sharing">
              <FiDownloadCloud />
            </a>
          </p>
        </div>
      </div>
    </div>
  );
}

export default About;
