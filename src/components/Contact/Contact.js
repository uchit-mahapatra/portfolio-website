import React, { useState } from "react";
import emailjs from "emailjs-com";
import "./Contact.scss";
import EmojiEmotionsIcon from "@material-ui/icons/EmojiEmotions";
import MailIcon from "@material-ui/icons/Mail";
import PhoneIcon from "@material-ui/icons/Phone";
import TwitterIcon from "@material-ui/icons/Twitter";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import GitHubIcon from "@material-ui/icons/GitHub";
import FacebookIcon from "@material-ui/icons/Facebook";

function Contact() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const contacts = [
    { url: "#contactMe", icon: <MailIcon />, id: "uchitmahapatra54@gmail.com" },
    { url: "#contactMe", icon: <PhoneIcon />, id: "+91-9337631547" },
  ];
  const socialHandle = [
    /* {
      url: "https://www.facebook.com/uchit.mahapatra.1",
      icon: <FacebookIcon />,
      id: "uchit.mahapatra.1",
    }, */

    {
      url: "https://www.github.com/horrible-coder/",
      icon: <GitHubIcon />,
      id: "horrible-coder",
    },
    {
      url: "https://www.linkedin.com/in/uchit-mahapatra-320687166/",
      icon: <LinkedInIcon />,
      id: "uchit-mahapatra-320687166",
    },
    {
      url: "https://twitter.com/UchitMahapatra",
      icon: <TwitterIcon />,
      id: "UchitMahapatra",
    },
  ];
  const handleName = (event) => {
    setName(event.target.value);
  };
  const handleEmail = (event) => {
    setEmail(event.target.value);
  };
  const handleMessage = (event) => {
    setMessage(event.target.value);
  };
  const handleSubmit = () => {
    console.log(name + " " + email + " " + message);
    let templateParams = {
      from_name: name,
      to_name: "Uchit",
      from_email: email,
      message: message,
    };
    emailjs.send(
      "default_service",
      "template_wkozrko",
      templateParams,
      "user_5RWrq5YreQBCfZF8ITEid"
    );
    alert("Message sent.");
    resetForm();
  };
  const resetForm = () => {
    setName("");
    setEmail("");
    setMessage("");
  };
  return (
    <div className="contact" id="contactMe">
      <h1>
        <span>Contact Me</span>
      </h1>
      <div className="contactMeContainer">
        <div className="contactMeForm">
          <div className="contactMeMessage">
            <p>Don't hesitate to say hey!</p>
            <EmojiEmotionsIcon />
          </div>
          <input
            type="text"
            value={name}
            onChange={handleName}
            placeholder="Name"
          />
          <input
            type="text"
            value={email}
            onChange={handleEmail}
            placeholder="Email"
          />
          <textarea
            value={message}
            onChange={handleMessage}
            placeholder="Message"
            rows="5"
          ></textarea>
          <button onClick={handleSubmit}>Send Message</button>
        </div>
        <div className="contactMeInfo">
          <p className="contactReachMe">Reach me at</p>
          {contacts.map((contact, index) => (
            <div className="contactList" key={index}>
              {contact.icon}
              <p>{contact.id}</p>
            </div>
          ))}
          <div className="socialHandleList">
            {socialHandle.map((contact, index) => (
              <a key={index} href={contact.url}>
                {contact.icon}
              </a>
            ))}{" "}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Contact;
