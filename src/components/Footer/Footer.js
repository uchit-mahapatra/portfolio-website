import React from "react";
import "./Footer.scss";

function Footer() {
  return (
    <div className="footer">
      <p>Developed and designed by Uchit Mahapatra.</p>
    </div>
  );
}

export default Footer;
