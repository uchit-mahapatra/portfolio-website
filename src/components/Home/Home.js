import React from "react";
import "./Home.scss";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";

function Home() {
  return (
    <div className="home" id="">
      <div className="homeContainer">
        <div className="leftContainer"></div>
        <div className="rightContainer">
          <div className="introduction">
            <h4>Hello,</h4>
            <h1>I'm Uchit</h1>
            <h3>Web developer</h3>
          </div>
        </div>
      </div>
{/* 
      <div className="scrollDownButton">
        <a href="#about">
          <img
            src="https://lh3.googleusercontent.com/proxy/kNblhWSbUTcNmgUFt2KDqqzsSrSdskZhM9LGBoHEu468AmpDagefCvt6TNkDOBQxn9MoxDzWUrX6e0Q_jPNcjqiDqh7A-Pt07UZnaZP0kSJkaiuOsQyx9gi_lEXDaHZtBDg-"
            alt="scrollDownGif"
          />
        </a>
      </div> */}
    </div>
  );
}

export default Home;
