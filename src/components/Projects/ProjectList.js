import EatTasty from "../../assets/eat-tasty.png";
import MemoryGame from "../../assets/memory-game.png";
import ChatApp from "../../assets/chat-app.png";
import StatusShare from "../../assets/status-share.png";
import TaskBoard from "../../assets/task-board.png";

const projectList = [
  {
    image: EatTasty,
    title: "Eat Tasty",
    description:
      "Food ordering app to order food from the best restaurants spread across the cities.",
    demoLink: "https://eat-tasty.netlify.app/",
    codeLink: "https://github.com/horrible-coder/eat-tasty",
    tags: ["React", "Redux", "NodeJS", "MongoDB", "HTML", "SCSS"],
  },
  {
    image: ChatApp,
    title: "Gossipp",
    description:
      "Real-time chat application that allows users to send messages to one another.",
    demoLink: "https://gossipp.netlify.app/",
    codeLink: "https://github.com/horrible-coder/gossipp",
    tags: ["React", "Redux", "NodeJS", "Socket.io", "HTML", "SCSS"],
  },
  {
    image: TaskBoard,
    title: "Task Board",
    description:
      "Visual collaboration tool to organize tasks and collaborate with team members.",
    demoLink: "",
    codeLink: "https://github.com/horrible-coder/task-board",
    tags: ["React", "NodeJS", "PostgreSQL", "Redux", "HTML", "SCSS"],
  },
  {
    image: StatusShare,
    title: "Status Share",
    description:
      "Social media application to share texts or images with your loved ones.",
    demoLink: "https://status-share.netlify.app/",
    codeLink: "https://github.com/horrible-coder/status-share",
    tags: ["React", "NodeJS", "GraphQL", "MongoDB", "HTML", "SCSS"],
  },

  {
    image: MemoryGame,
    title: "PokeMatch",
    description:
      "Test your memory and find the pair of matching pokemons with minimum no. of attempts and time.",
    demoLink: "https://poke-match.netlify.app/",
    codeLink: "https://bitbucket.org/uchit-mahapatra/memory-game/src",
    tags: ["React", "HTML", "SCSS"],
  },
];

export default projectList;
