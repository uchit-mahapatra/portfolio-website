import React, { useState } from "react";
import "./Projects.scss";
import { Button } from "@material-ui/core";
import ProjectList from "./ProjectList";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";
import "@splidejs/splide/dist/css/themes/splide-sea-green.min.css";
import "@splidejs/splide/dist/css/themes/splide-skyblue.min.css";

function Projects() {
  const mediaQueryLarge = window.matchMedia("(min-width: 1080px)");
  const mediaQueryMedium = window.matchMedia(
    "(min-width: 750px) and (max-width: 1080px)"
  );
  const mediaQuerySmall = window.matchMedia(
    "(min-width: 450px) and (max-width: 750px)"
  );
  const mediaQueryExtraSmall = window.matchMedia("(max-width: 450px)");

  const [mediaScreenLarge, setMediaScreenLarge] = useState(
    mediaQueryLarge.matches
  );
  const [mediaScreenMedium, setMediaScreenMedium] = useState(
    mediaQueryMedium.matches
  );
  const [mediaScreenSmall, setMediaScreenSmall] = useState(
    mediaQuerySmall.matches
  );
  const [mediaScreenExtraSmall, setMediaScreenExtraSmall] = useState(
    mediaQueryExtraSmall.matches
  );

  const handleScreenSizeChangeLarge = (e) => {
    e.matches ? setMediaScreenLarge(true) : setMediaScreenLarge(false);
  };
  const handleScreenSizeChangeMedium = (e) => {
    e.matches ? setMediaScreenMedium(true) : setMediaScreenMedium(false);
  };
  const handleScreenSizeChangeSmall = (e) => {
    e.matches ? setMediaScreenSmall(true) : setMediaScreenSmall(false);
  };
  const handleScreenSizeChangeExtraSmall = (e) => {
    e.matches
      ? setMediaScreenExtraSmall(true)
      : setMediaScreenExtraSmall(false);
  };

  mediaQueryLarge.addEventListener("change", handleScreenSizeChangeLarge);
  mediaQueryMedium.addEventListener("change", handleScreenSizeChangeMedium);
  mediaQuerySmall.addEventListener("change", handleScreenSizeChangeSmall);
  mediaQueryExtraSmall.addEventListener(
    "change",
    handleScreenSizeChangeExtraSmall
  );

  return (
    <div className="projects" id="projects">
      <h1>
        <span>Projects</span>
      </h1>
      <div className="projectsContainer">
        <Splide
          options={
            (mediaScreenLarge && {
              perPage: 3,
              width: "80%",
            }) ||
            (mediaScreenMedium && {
              perPage: 2,
              width: "80%",
            }) ||
            (mediaScreenSmall && {
              perPage: 1,
              width: "50%",
            }) ||
            (mediaScreenExtraSmall && {
              perPage: 1,
              width: "75%",
            })
          }
        >
          {ProjectList.map((project, index) => (
            <SplideSlide key={index}>
              <div className="project">
                <img src={project.image} alt={project.title} />
                <p className="projectTitle">{project.title}</p>
                <p className="projectDescription">{project.description}</p>
                <div className="projectTags">
                  {project.tags.map((tags, index) => (
                    <div className="tag" key={index}>
                      <p>{tags}</p>
                    </div>
                  ))}
                </div>
                <Button
                  href={project.demoLink}
                  target="_blank"
                  className="demoButton"
                >
                  View Demo
                </Button>
                <Button
                  href={project.codeLink}
                  target="_blank"
                  className="codeButton"
                >
                  View Code
                </Button>
              </div>
            </SplideSlide>
          ))}
        </Splide>
      </div>
    </div>
  );
}

export default Projects;
