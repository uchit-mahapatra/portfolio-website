import React, { useEffect, useRef, useState } from "react";
import "./Sidebar.scss";
import SidebarItems from "./SidebarItems";

function Sidebar({ pos }) {
  const homeRef = pos[0],
    aboutRef = pos[1],
    skillsRef = pos[2],
    projectsRef = pos[3],
    contactRef = pos[4];

  const sidebarRef = useRef(null);

  const [iconNumber, setIconNumber] = useState(0);

  useEffect(() => {
    const aboutTopPosition = aboutRef.current.offsetTop;
    const skillsTopPosition = skillsRef.current.offsetTop;
    const projectsTopPosition = projectsRef.current.offsetTop;
    const contactTopPosition = contactRef.current.offsetTop;

    const onScroll = () => {
      const scrollPosition = window.scrollY + 300;
      if (scrollPosition < aboutTopPosition) {
        setIconNumber(0);
      } else if (
        aboutTopPosition <= scrollPosition &&
        scrollPosition < skillsTopPosition
      ) {
        setIconNumber(1);
      } else if (
        skillsTopPosition <= scrollPosition &&
        scrollPosition < projectsTopPosition
      ) {
        setIconNumber(2);
      } else if (
        projectsTopPosition <= scrollPosition &&
        scrollPosition < contactTopPosition
      ) {
        setIconNumber(3);
      } else {
        setIconNumber(4);
      }
    };
    window.addEventListener("scroll", onScroll);
    return () => window.removeEventListener("scroll", onScroll);
  }, [homeRef, aboutRef, skillsRef, projectsRef, contactRef]);

  return (
    <div className="sidebar">
      <div className="sidebarButton">
        {SidebarItems.map((sidebarIcon, index) => (
          <div key={index}>
            <div
              className={
                index !== iconNumber ? "sidebarIcon" : "sidebarIconActive"
              }
              ref={sidebarRef}
            >
              <a href={sidebarIcon.href}>{sidebarIcon.icon}</a>
            </div>
            {SidebarItems[index + 1] && <div className="connectorLine"></div>}
          </div>
        ))}
      </div>
    </div>
  );
}

export default Sidebar;
