import HomeIcon from "@material-ui/icons/Home";
import PersonIcon from "@material-ui/icons/Person";
import AssignmentIcon from "@material-ui/icons/Assignment";
import MailIcon from "@material-ui/icons/Mail";
import BuildIcon from "@material-ui/icons/Build";

const sidebarItems = [
  {
    icon: <HomeIcon />,
    name: "Home",
    href: "#",
  },
  {
    icon: <PersonIcon />,
    name: "About",
    href: "#about",
  },
  {
    icon: <BuildIcon />,
    name: "Skills",
    href: "#skills",
  },
  {
    icon: <AssignmentIcon />,
    name: "Projects",
    href: "#projects",
  },
  {
    icon: <MailIcon />,
    name: "Contact Me",
    href: "#contactMe",
  },
];

export default sidebarItems;
