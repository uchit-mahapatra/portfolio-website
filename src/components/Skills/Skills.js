import React from "react";
import "./Skills.scss";
import Languages from "./SkillsItems";

function Skills() {
  return (
    <div className="skills" id="skills">
      <h1>
        <span>Skills</span>
      </h1>
      <div className="skillsContainer">
        {Languages.map((language, index) => (
          <div className="language" key={index}>
            <div className="languageImage">{language.image}</div>
            <p className="languageName">{language.name}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Skills;
