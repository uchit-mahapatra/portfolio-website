import {
  SiCplusplus,
  SiJava,
  SiJavascript,
  SiHtml5,
  SiCss3,
  SiPython,
  SiReact,
  SiRedux,
  SiNodeDotJs,
  SiMysql,
  SiMongodb,
  SiFlask,
  SiJquery,
  SiAdobeillustrator,
  SiAdobephotoshop,
  SiGraphql,
} from "react-icons/si";
const languages = [
  {
    image: <SiCplusplus />,
    name: "C++",
  },
  {
    image: <SiJavascript />,
    name: "JavaScript",
  },
  {
    image: <SiJava />,
    name: "Java",
  },
  {
    image: <SiPython />,
    name: "Python",
  },
  {
    image: <SiFlask />,
    name: "Flask",
  },
  {
    image: <SiReact />,
    name: "React",
  },
  {
    image: <SiRedux />,
    name: "Redux",
  },
  {
    image: <SiNodeDotJs />,
    name: "Node.js",
  },
  {
    image: <SiMysql />,
    name: "MySQL",
  },
  {
    image: <SiMongodb />,
    name: "MongoDB",
  },
  {
    image: <SiHtml5 />,
    name: "HTML",
  },
  {
    image: <SiCss3 />,
    name: "CSS",
  },
  {
    image: <SiGraphql />,
    name: "GraphQL",
  },
  {
    image: <SiAdobeillustrator />,
    name: "Adobe Illustrator",
  },
  {
    image: <SiAdobephotoshop />,
    name: "Adobe Photoshop",
  },
];
export default languages;
